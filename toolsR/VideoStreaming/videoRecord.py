# !/usr/bin/python3
# -*- coding: utf-8 -*-

from time import gmtime, strftime, sleep

import cv2
import multiprocessing, os
import numpy as np
from toolsR.utils import TimerR
from toolsR.VideoTracking.algorithm import Algorithm
from toolsR.VideoTracking.settings import *

_commandQueue = multiprocessing.JoinableQueue()

class VideoR:

    def __init__(self, indx_or_path=0, name_video=None, path=None, width=None, height=None, fps=None, codec_cam='MJPG',
                 codec_video='MJPG', showWindows=True, tracking=False, nAreas=3, bpod_address=None):
        
        # Control
        if type(indx_or_path) is not int:
            raise ValueError('VideoR: The indx_or_path has to be an Integer, it is 0 as default.')
        if type(name_video) is not str and name_video is not None:
            raise ValueError('VideoR: The name_video has to be a String.')
        if type(path) is not str and path is not None:
            raise ValueError('VideoR: The path has to be a String.')
        elif path is not None:
            if not os.path.exists(path):
                raise ValueError('VideoR: The path = ' + path + ' does not exist.')
        if type(width) is not int and width is not None:
            raise ValueError('VideoR: The width has to be an Integer.')
        if type(height) is not int and height is not None:
            raise ValueError('VideoR: The height has to be an Integer.')
        if type(fps) is not int and fps is not None:
            raise ValueError('VideoR: The fps has to be an Integer.')
        if type(codec_cam) is not str:
            raise ValueError('VideoR: The codec_cam has to be a String, it is MJPG as default.')
        if type(codec_video) is not str:
            raise ValueError('VideoR: The codec_video has to be a String,  it is MJPG as default.')
        if type(showWindows) is not bool:
            raise ValueError('VideoR: The flag has to be a Boolean, it is True as default.')
        if type(tracking) is not bool:
            raise ValueError('VideoR: The flag has to be a Boolean, it is False as default.')
        if type(nAreas) is not int:
            raise ValueError('VideoR: The number of areas has to be an Integer.')
        if type(bpod_address) is not int and bpod_address is not None:
            raise ValueError('VideoR: The address has to be integer. Check the address of Bpod.')


        self._idx = indx_or_path
        self._nv = name_video
        self._pt = path
        self._wt = width
        self._hg = height
        self._fps = fps
        self._codecCamera = codec_cam
        self._codecVideo = codec_video
        self._consumer = None
        self._showW = showWindows

        # Softcode tracking
        self.tracking = tracking
        self.nAreas = nAreas
        self.address = bpod_address

    def get_fps(self):
        test = False
        try:
            test = self._consumer.is_alive()
        except AttributeError:
            print('VideoR: The camera does not start. Use the method play() before record.')
        if test:
            return self._consumer.get_fps()
        else:
            print('VideoR: The camera object was closed.')


    def get_codec(self):
        test = False
        try:
            test = self._consumer.is_alive()
        except AttributeError:
            print('VideoR: The camera does not start. Use the method play() before record.')
        if test:
            return self._consumer.get_codec()
        else:
            print('VideoR: The camera object was closed.')

    def get_size(self):
        test = False
        try:
            test = self._consumer.is_alive()
        except AttributeError:
            print('VideoR: The camera does not start. Use the method play() before record.')
        if test:
            return self._consumer.get_size()
        else:
            print('VideoR: The camera object was closed.')

    def play(self):
        global _commandQueue
        test = False
        try: # if it the first time it should catch the exception and create the process
            test = self._consumer.is_alive()
        except AttributeError:
            self._consumer = MultiprocR(_commandQueue, self._idx, self._nv, self._pt, self._wt,
                                        self._hg, self._fps, self._codecCamera, self._codecVideo, self._showW,
                                        self.tracking, self.nAreas, self.address)
            self._consumer.start()
            return

        if test:
            print('VideoR: The camera is already running.')

    def record(self):
        global _commandQueue
        test = False
        try:
            test = self._consumer.is_alive()
        except AttributeError:
            print('VideoR: The camera does not start. Use the method play() before record.')
        if test:
            _commandQueue.put(2)
        else:
            print('VideoR: The camera object was closed.')

    def pause(self):
        global _commandQueue
        test = False
        try:
            test = self._consumer.is_alive()
        except AttributeError:
            print('VideoR: The camera does not start. Use the method play() before record.')
        if test:
            _commandQueue.put(3)
        else:
            print('VideoR: The camera object was closed.')

    def stop(self):
        global _commandQueue
        test = False
        try:
            test = self._consumer.is_alive()
        except AttributeError:
            print('VideoR: The camera does not start. Use the method play() before record.')
        if test:
            _commandQueue.put(0)
            self._consumer.join()
            self._consumer = None
        else:
            print('VideoR: The camera object was closed.')

    def isOpen(self):
        return self._consumer.is_alive()


class MultiprocR(multiprocessing.Process):

    def __init__(self, command_queue, idx_camera, name_video=None, path=None, width=None, height=None,
                 fps=None, fourcc_cam = None, fourcc_video = None, showWin = True, tracking=False,  nAreas=3, address=None):

        multiprocessing.Process.__init__(self)
        self.command_queue = command_queue

        self.video = cv2.VideoCapture(idx_camera)
        self.name_video = name_video
        self.path = path
        self.fps = []
        self.width = []
        self.height = []
        self.isSaving = False # is True only when start to record
        self.recordV = -1
        self.frame_counter = 0
        self.showW = showWin

        fourcc_cam = cv2.VideoWriter_fourcc(*fourcc_cam)
        self.video.set(cv2.CAP_PROP_FOURCC, fourcc_cam)

        if width is not None:
            self.video.set(cv2.CAP_PROP_FRAME_WIDTH, width)
        if height is not None:
            self.video.set(cv2.CAP_PROP_FRAME_HEIGHT, height)
        if fps is not None:
            self.video.set(cv2.CAP_PROP_FPS, fps)

        self.fps = int(self.video.get(cv2.CAP_PROP_FPS))
        self.width = int(self.video.get(cv2.CAP_PROP_FRAME_WIDTH))
        self.height = int(self.video.get(cv2.CAP_PROP_FRAME_HEIGHT))

        # Record the video
        self.fourccOut = cv2.VideoWriter_fourcc(*fourcc_video)

        if name_video is None:
            self.name_video = strftime("%d%b%Y_%H%M%S.mkv", gmtime())
        if path is None:
            self.path = ''

        self.outVideo = None # It opens a channel to save the video only when the variable isSaving=true

        self.frame = np.zeros([self.height, self.width])

        # Softcode tracking
        self.alg = Algorithm(nAreas, address)
        self.tracking = tracking

    def run(self):

        chrono = TimerR()

        # fps counter
        fps_now     = 0
        counter_fps = 0 
        last_sec    = 0 

        # Start
        next_command = 1
        while True:
            # Get the command
            if not self.command_queue.empty():
                next_command = self.command_queue.get()

            # Check the command and change the variables
            if next_command == 2:
                self.recordV = 2
                if not self.isSaving:
                    self.outVideo = cv2.VideoWriter(self.path + self.name_video, self.fourccOut, self.fps, (self.width, self.height))
                    self.isSaving = True
            elif next_command == 3:
                chrono.pause()
                self.recordV = 3
            elif next_command == 0:
                if chrono.isRunning():  # close the timer
                    chrono.stop()
                    chrono.reset()
                self.recordV = 0

            if self.recordV == 0:
                break
            else:
                a, frame = self.video.read()
                if a:
                    if self.recordV == 2:
                        self.outVideo.write(frame)
                        self.frame_counter  += 1
                        counter_fps += 1

                        if not chrono.isRunning() or chrono.isPause(): # Start the chrono
                            chrono.start()

                        now_sec = int(chrono.getTimeSeconds())
                        if now_sec % 5 == 0 and now_sec != last_sec: # reset the fps counter each 5 sec
                            last_sec = int(now_sec)
                            fps_now = int(counter_fps / 5)
                            counter_fps = 0
                            print(last_sec)

                        cv2.putText(frame, 'fps: ' + str(fps_now) + ' time: ' + chrono.getString() + ' Rec',
                                    (10, 30),
                                    cv2.FONT_HERSHEY_DUPLEX, 0.5, (255, 255, 255), 1, cv2.LINE_AA)
                        if self.tracking:
                            mask, frame = self.alg.apply(frame)
                    elif self.recordV == 3:
                        if chrono.isRunning():
                            cv2.putText(frame, 'fps: ' + str(fps_now)+ ' time: ' + chrono.getString() + ' Pause',
                                        (10, 30), cv2.FONT_HERSHEY_DUPLEX, 0.5, (255, 255, 255), 1, cv2.LINE_AA) 
                    if self.showW:
                        cv2.imshow("Streaming Camera", frame)
                else:
                    raise IndexError('VideoR: Camera did not find. Check the connection and the index.')

            # Exit if ESC pressed
            if self.showW:
                k = cv2.waitKey(1) & 0xff
                if k == 27:
                    break

        self.close()
        return

    def close(self): # release the camera and close the windows, then save the file.
        self.video.release()
        cv2.destroyAllWindows()
        if self.outVideo is not None:
            self.outVideo.release()
            print('VideoR: Video saved.')
            print('Total Frames = ', self.frame_counter)

    def get_fps(self):
        return int(self.video.get(cv2.CAP_PROP_FPS))

    def get_size(self):
        return [self.width,self.height]

    def get_codec(self):
        return self._decode_fourcc(self.video.get(cv2.CAP_PROP_FOURCC))

    def _decode_fourcc(self, v):
        v = int(v)
        return "".join([chr((v >> 8 * i) & 0xFF) for i in range(4)])