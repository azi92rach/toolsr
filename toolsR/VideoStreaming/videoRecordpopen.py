# !/usr/bin/python3
# -*- coding: utf-8 -*-

import subprocess, os
import toolsR

class VideoRP():
    def __init__(self, idx_camera=0, name_video=None, path=None, width=None, height=None, fps=None, codec_cam='MJPG',
                 codec_video='MJPG', showWindows = True):

        # Control
        if type(idx_camera) is not int:
            raise ValueError('VideoRP: The idx_camera has to be an Integer, it is 0 as default.')
        if type(name_video) is not str and name_video is not None:
            raise ValueError('VideoRP: The name_video has to be a String.')
        if type(path) is not str and path is not None:
            raise ValueError('VideoRP: The path has to be a String.')
        elif path is not None:
            if not os.path.exists(path):
                raise ValueError('VideoRP: The path = ' + path + ' does not exist.')
        if type(width) is not int and width is not None:
            raise ValueError('VideoRP: The width has to be an Integer.')
        if type(height) is not int and height is not None:
            raise ValueError('VideoRP: The height has to be an Integer.')
        if type(fps) is not int and fps is not None:
            raise ValueError('VideoRP: The fps has to be an Integer.')
        if type(codec_cam) is not str:
            raise ValueError('VideoRP: The codec_cam has to be a String, it is MJPG as default.')
        if type(codec_video) is not str:
            raise ValueError('VideoRP: The codec_video has to be a String,  it is MJPG as default.')
        if type(showWindows) is not bool:
            raise ValueError('VideoRP: The index has to be a Boolean, it is True as default.')

        # Parameters
        _prts = ['-i' + str(idx_camera), '-n' + str(name_video), '-p' + str(path), '-w' + str(width),
                '-g' + str(height), '-f' + str(fps), '-c' + str(codec_cam), '-d' + str(codec_video),
                '-s' + str(showWindows)]

        _pathScript = toolsR.__path__[0] + '/connectVideo.py'
        self._process = subprocess.Popen(['python3', _pathScript, _prts[0], _prts[1], _prts[2], _prts[3], _prts[4], _prts[5],
                                          _prts[6], _prts[7], _prts[8]], stdin=subprocess.PIPE, stdout=subprocess.PIPE)

        self._isRunning = False
        self._isRecording = False

    def play(self):
        try:
            if self._isRunning:
                print('VideoRP: The video is running.')
                return
            self._isRunning = True
            self._process.stdin.write(b"P\n")
            self._process.stdin.flush()
        except BrokenPipeError:
            print('VideoRP: closed. Open a new VideoRP Object.')

    def record(self):
        try:
            if self._isRecording:
                print('VideoRP: The video is recording.')
                return
            self._process.stdin.write(b"R\n")
            self._process.stdin.flush()
        except BrokenPipeError:
            print('VideoRP closed. Open a new VideoRP Object.')

    def pause(self):
        try:
            if not self._isRecording:
                print('VideoRP: The video is not recording.')
                return
            self._process.stdin.write(b"P\n")
            self._process.stdin.flush()
        except BrokenPipeError:
            print('VideoRP closed. Open a new VideoRP Object.')

    def stop(self):
        try:
            if not self._isRunning:
                print('VideoRP: No video is running.')
                return
            self._process.stdin.write(b"S\n")
            self._process.stdin.flush()
        except BrokenPipeError:
            print('VideoRP closed. Open a new VideoRP Object.')
